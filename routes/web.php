<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*-- pageController --*/
	Route::get('/', 'pageController@logInForm');
	Route::post('/logIn', 'pageController@logIn');
	Route::get('/main', 'pageController@mainMenu')->middleware('isLoged');
	Route::get('/users', 'pageController@users')->middleware('isLoged', 'isAdmin');
	Route::get('/reports', 'pageController@reports')->middleware('isLoged');
	Route::get('/logOut', 'pageController@logOut')->middleware('isLoged');

/*-- employeeController --*/
	Route::get('/getReportsByUserId', 'employeeController@getReportsByUserId')->middleware('isLoged');
	Route::get('/getReportById', 'employeeController@getReportById')->middleware('isLoged');
	Route::post('/reportUpdate', 'employeeController@reportUpdate')->middleware('isLoged');
	Route::post('/reportDelete', 'employeeController@reportDelete')->middleware('isLoged');
	Route::post('/saveNewReport', 'employeeController@saveNewReport')->middleware('isLoged');

/*-- adminController --*/
	Route::get('getUsers', 'adminController@getUsers')->middleware('isLoged', 'isAdmin');
	Route::get('getRoles', 'adminController@getRoles')->middleware('isLoged', 'isAdmin');
	Route::post('userSave', 'adminController@userSave')->middleware('isLoged', 'isAdmin');
	Route::get('getUserById', 'adminController@getUserById')->middleware('isLoged', 'isAdmin');
	Route::post('userUpdate', 'adminController@userUpdate')->middleware('isLoged', 'isAdmin');
	Route::post('userDelete', 'adminController@userDelete')->middleware('isLoged', 'isAdmin');

