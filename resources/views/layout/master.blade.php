<!DOCTYPE html>

<html>
    <head>
        <title>Mensore Studio - @yield('title')</title>
		<link type="text/css" rel="stylesheet" href="./css/app.css"/>
		<link type="text/css" rel="stylesheet" href="./css/style.css"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<script type="text/javascript" src="./js/app.js"></script>
		<script type="text/javascript" src="./js/functions.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">Mensore Studio</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span><i class="fas fa-user"></i> {{ session()->get('user') }}</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="./reports"><i class="fas fa-file-alt"></i> Reportes</a>
							@if(session()->get('role') == 'admin')
								<a class="dropdown-item" href="./users"><i class="fas fa-users"></i> Usuarios</a>
							@endif
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="./logOut"><i class="fas fa-sign-out-alt"></i> Salir</a>
						</div>
					</li>
				</ul>
			</div>
		</nav>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>