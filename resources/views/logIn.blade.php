<!DOCTYPE html>

<html>
	<head>
		<title>Employee Test | Login</title>
		<link type="text/css" rel="stylesheet" href="./css/app.css"/>
		<link type="text/css" rel="stylesheet" href="./css/style.css"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<script type="text/javascript" src="./js/app.js"></script>
	</head>
	<body>
		<div class="row login-container">
			<div class="login">
				<h1 class="text-center">Mensore Studio LogIn</h1>
				@if(session()->has('message'))
					<div class="alert alert-success fade-in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('message') }}
					</div>
				@endif
				
				<form method="POST" action="./logIn">
					@csrf
					<div class="form-group">
						<label for="email">Email: </label>
						<input type="email" name="email" id="email" class="form-control"/>
					</div>
					<div class="form-group">
						<label for="password">Contraseña: </label>
						<input type="password" name="password" id="password" class="form-control"/>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-success float-right" value="Ingresar"/>
					</div>
				</form>
				
				@if(session()->has('error'))
					<div class="alert alert-danger fade-in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ session()->get('error') }}
					</div>
				@endif
			</div>
		</div>
	</body>
</html>