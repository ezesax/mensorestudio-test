@extends('layout.master')
	@section('title', 'Principal')
	
	@section('content')
		<h1>Usuarios</h1>
		<button onclick="getRoles()" class="btn btn-success float-right" data-toggle="modal" data-target="#newUserModal"><i class="fas fa-user-plus"></i> Nuevo usuario</button>
		<div id="gridResult">
			<table class="table">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>Correo</th>
						<th>Rol</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="tbody">
				</tbody>
			</table>
		</div>
		<div class="modal fade" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="newUserModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="newUser">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="newUserModalLabel">Nuevo Usuario</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="userName">Usuario: </label>
								<input type="text" class="form-control" id="userName" name="userName"/>
							</div>
							<div class="form-group">
								<label for="userEmail">Email: </label>
								<input type="email" class="form-control" id="userEmail" name="userEmail"/>
							</div>
							<div class="form-group">
								<label for="userRole">Rol: </label>
								<select class="form-control" id="userRole" name="userRole"/></select>
							</div>
							<div class="form-group">
								<label for="userPassword">Contraseña: </label>
								<input type="password" class="form-control" id="userPassword" name="userPassword"/>
							</div>
							<div class="form-group">
								<label for="userRePassword">Repetir contraseña: </label>
								<input type="password" class="form-control" id="userRePassword" name="userRePassword"/>
							</div>
						</div>
						<div id="newUserError">							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="userSave()" type="button" class="btn btn-success">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="userEdit" tabindex="1" role="dialog" aria-labelledby="userEditLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="updateUser">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="userEditLabel">Editar Usuario</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="userEditName">Usuario: </label>
								<input type="text" class="form-control" id="userEditName" name="userEditName"/>
							</div>
							<div class="form-group">
								<label for="userEditEmail">Email: </label>
								<input type="email" class="form-control" id="userEditEmail" name="userEditEmail"/>
							</div>
							<div class="form-group">
								<label for="userEditRole">Rol: </label>
								<select class="form-control" id="userEditRole" name="userEditRole"/></select>
							</div>
							<div class="form-group">
								<label for="userEditPassword">Contraseña: </label>
								<input type="password" class="form-control" id="userEditPassword" name="userEditPassword"/>
							</div>
							<div class="form-group">
								<label for="userEditRePassword">Repetir contraseña: </label>
								<input type="password" class="form-control" id="userEditRePassword" name="userEditRePassword"/>
							</div>
							<input type="hidden" name="userEditId" id="userEditId"/>
						</div>
						<div id="updateUserError">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="userUpdate()" type="button" class="btn btn-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="userDeleteConfirm" tabindex="1" role="dialog" aria-labelledby="userDeleteConfirmLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="deleteUser">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="userDeleteConfirmLabrl">Eliminar</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<h6>¿Está seguro que desea eliminar a <b><span id="userDeleteName"></span></b>?</h6>
							<input type="hidden" name="userDeleteId" id="userDeleteId"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="userDelete()" type="button" class="btn btn-danger">Eliminar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>
			var hola = '';
			$.ajax({
				url:'./getUsers',
				type:'GET',
				dataType: 'json',
				success: function(res){
					hola = res;
					for(var i = 0; i < res.users.length; i++){
						var tr = '<tr id="tr-'+res.users[i].id+'">';
						tr += '<td id="user-'+res.users[i].id+'">'+res.users[i].user+'</td>';
						tr += '<td id="email-'+res.users[i].id+'">'+res.users[i].email+'</td>';
						tr += '<td id="role-'+res.users[i].id+'">'+res.users[i].roleName+'</td>';
						tr += '<td><button class="btn btn-primary" onclick="userEdit('+res.users[i].id+')"><i class="fas fa-user-edit"></i></button><button class="btn btn-danger" onclick="userDeleteConfirm('+res.users[i].id+')"><i class="fas fa-user-times"></i></button></td>';
						tr += '</tr>';
						
						var tbody = $('#tbody').html();
						tbody += tr;
						$('#tbody').html(tbody);
					}
				},
				error: function(er){
					console.log(er)
				}
			});
		</script>
	@endsection