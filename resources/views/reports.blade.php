@extends('layout.master')
	@section('title', 'Principal')
	
	@section('content')
		<h1>Mis reportes</h1>
		<button class="btn btn-success float-right" data-toggle="modal" data-target="#newReportModal"><i class="fas fa-plus"></i> Nuevo reporte</button>
		<div id="gridResult">
			<table class="table">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Descripción</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="tbody">
				</tbody>
			</table>
		</div>
		<div class="modal fade" id="newReportModal" tabindex="-1" role="dialog" aria-labelledby="newReportModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="newReport">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="newReportModalLabel">Nuevo Reporte</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="reportDescription">Descripción: </label>
								<textarea class="form-control" id="reportDescription" name="reportDescription"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="reportSave()" type="button" class="btn btn-success">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="reportEdit" tabindex="1" role="dialog" aria-labelledby="reportEditLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="updateReport">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="reportEditLabel">Editar Reporte</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="reportEditDescription">Descripción: </label>
								<textarea class="form-control" id="reportEditDescription" name="reportEditDescription"></textarea>
							</div>
							<input type="hidden" id="reportId" name="reportId"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="reportUpdate()" type="button" class="btn btn-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="reportDeleteConfirm" tabindex="1" role="dialog" aria-labelledby="reportDeleteConfirmLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="deleteReport">
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="reportDeleteConfirmLabel">Eliminar</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<h6>¿Está seguro que desea eliminar este reporte?</h6>
							<input type="hidden" name="reportId" id="reportIdToDelete"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button onclick="reportDelete()" type="button" class="btn btn-danger">Eliminar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script>
			$.ajax({
				url:'./getReportsByUserId',
				type:'GET',
				dataType: 'json',
				success: function(res){
					for(var i = 0; i < res.reports.length; i++){
						var tr = '<tr id="tr-'+res.reports[i].id+'">';
						tr += '<td>'+res.reports[i].date.substring(0, 10)+'</td>';
						tr += '<td id="des-'+res.reports[i].id+'">'+res.reports[i].description.substring(0, 100)+'...</td>';
						tr += '<td><button class="btn btn-primary" onclick="reportEdit('+res.reports[i].id+')"><i class="fas fa-edit"></i></button><button class="btn btn-danger" onclick="reportDeleteConfirm('+res.reports[i].id+')"><i class="fas fa-trash"></i></button></td>';
						tr += '</tr>';
						
						var tbody = $('#tbody').html();
						tbody += tr;
						$('#tbody').html(tbody);
					}
				},
				error: function(er){
					console.log(er)
				}
			});
		</script>
	@endsection