'use strict'

//Employee functions//

function reportSave(){
	var newReportDescription = $('#reportDescription').val();
	
	$.ajax({
		url:'./saveNewReport',
		type:'POST',
		dataType: 'json',
		data: $("#newReport").serialize(),
		success: function(res){
			var tr = '<tr id="tr-'+res.reportId+'">';
			tr += '<td>'+res.date.substring(0, 10)+'</td>';
			tr += '<td id="des-'+res.reportId+'">'+newReportDescription.substring(0, 100)+'...</td>';
			tr += '<td><button class="btn btn-primary" onclick="reportEdit('+res.reportId+')"><i class="fas fa-edit"></i></button><button class="btn btn-danger" onclick="reportDeleteConfirm('+res.reportId+')"><i class="fas fa-trash"></i></button></td>';
			tr += '</tr>';
			
			var tbody = $('#tbody').html();
			tbody += tr;
			$('#tbody').html(tbody);
			
			$('#reportDescription').val('');
			
			$('#newReportModal').modal('hide');
		},
		error: function(er){
			console.log(er)
		}
	});
}

function reportEdit(reportId){
	$('#reportId').val(reportId);
	
	$.ajax({
		url:'./getReportById',
		type:'GET',
		dataType: 'json',
		data: {
			'reportId' : reportId
		},
		success: function(res){
			$('#reportId').val(reportId);
			$('#reportEdit').modal();
			$('#reportEditDescription').val(res.report.description);
		},
		error: function(er){
			console.log(er)
		}
	});
}

function reportUpdate(){
	var newDescription = $('#reportEditDescription').val();
	var reportId = $('#reportId').val();
	
	$.ajax({
		url:'./reportUpdate',
		type:'POST',
		dataType: 'json',
		data: $("#updateReport").serialize(),
		success: function(res){
			$('#des-'+reportId).text(newDescription.substring(0, 100)+'...');
			$('#reportEditDescription').val('');
			$('#reportEdit').modal('hide');
		},
		error: function(er){
			console.log(er);
		}
	});
}

function reportDeleteConfirm(reportId){
	$('#reportIdToDelete').val(reportId);
	$('#reportDeleteConfirm').modal();
}

function reportDelete(){
	var reportId = $('#reportIdToDelete').val();
	
	$.ajax({
		url:'./reportDelete',
		type:'POST',
		dataType: 'json',
		data: $("#deleteReport").serialize(),
		success: function(res){
			$('#tr-'+reportId).remove();
			$('#reportDeleteConfirm').modal('hide');
		},
		error: function(er){
			console.log(er);
		}
	});
}

//****//

//Admin functions//

function getRoles(){
	$('#newUserError').html('');
	$('#newUserError').removeClass('fade-in alert alert-danger');
	$('#userName').val('');
	$('#userEmail').val('');
	$('#userPassword').val('');
	$('#userRePassword').val('');
				
	$.ajax({
		url:'./getRoles',
		type:'GET',
		dataType: 'json',
		success: function(res){
			$('#userRole').html('');
			for(var i = 0; i < res.roles.length; i++){
				var role = '<option value="'+res.roles[i].id+'">'+res.roles[i].name+'</option>';
				var options = $('#userRole').html();
				options += role;
				
				$('#userRole').html(options);
			}
		},
		error: function(er){
			console.log(er);
		}
	});
}

function userSave(){
	$.ajax({
		url:'./userSave',
		type:'POST',
		dataType: 'json',
		data: $("#newUser").serialize(),
		success: function(res){
			if(res.error == false){
				var tr = '<tr id="tr-'+res.user.id+'">';
				tr += '<td id="user-'+res.user.id+'">'+res.user.user+'</td>';
				tr += '<td id="email-'+res.user.id+'">'+res.user.email+'</td>';
				tr += '<td id="role-'+res.user.id+'">'+res.user.roleName+'</td>';
				tr += '<td><button class="btn btn-primary" onclick="userEdit('+res.user.id+')"><i class="fas fa-user-edit"></i></button><button class="btn btn-danger" onclick="userDeleteConfirm('+res.user.id+')"><i class="fas fa-user-times"></i></button></td>';
				tr += '</tr>';
				
				var tbody = $('#tbody').html();
				tbody += tr;
				$('#tbody').html(tbody);
				
				$('#userName').val('');
				$('#userEmail').val('');
				$('#userPassword').val('');
				$('#userRePassword').val('');
				
				$('#newUserModal').modal('hide');
			}else{
				$('#newUserError').html(res.message);
				$('#newUserError').addClass('fade-in alert alert-danger');
			}
		},
		error: function(er){
			console.log(er)
		}
	});
}

function userEdit(userId){
	$('#updateUserError').html('');
	$('#updateUserError').removeClass('fade-in alert alert-danger');
	$('#userEditId').val(userId);
	$.ajax({
		url:'./getUserById',
		type:'GET',
		dataType: 'json',
		data: {
			'userId' : userId
		},
		success: function(res){
			$('#userEditName').val(res.user.user);
			$('#userEditEmail').val(res.user.email);
			$('#userEditPassword').val(res.user.password);
			$('#userEditRePassword').val(res.user.password);
			
			$('#userEdit').modal();
			
			setRole(res.user.role_id);
		},
		error: function(er){
			console.log(er)
		}
	});
}

function setRole(currentRoleId){
	$.ajax({
		url:'./getRoles',
		type:'GET',
		dataType: 'json',
		success: function(res){
			$('#userEditRole').html('');
			for(var i = 0; i < res.roles.length; i++){
				if(res.roles[i].id == currentRoleId){
					var role = '<option value="'+res.roles[i].id+'" selected>'+res.roles[i].name+'</option>';
				}else{
					var role = '<option value="'+res.roles[i].id+'">'+res.roles[i].name+'</option>';
				}
				var options = $('#userEditRole').html();
				options += role;
				
				$('#userEditRole').html(options);
			}
		},
		error: function(er){
			console.log(er);
		}
	});
}

function userUpdate(){
	var userEditId = $('#userEditId').val();
	
	$.ajax({
		url:'./userUpdate',
		type:'POST',
		dataType: 'json',
		data: $("#updateUser").serialize(),
		success: function(res){
			if(res.error == false){
				$('#user-'+userEditId).text(res.user.user);
				$('#email-'+userEditId).text(res.user.email);			
				$('#role-'+userEditId).text(res.user.roleName);			
				
				$('#userEdit').modal('hide');
			}else{
				$('#updateUserError').html(res.message);
				$('#updateUserError').addClass('fade-in alert alert-danger');
			}
		},
		error: function(er){
			console.log(er);
		}
	});
}

function userDeleteConfirm(userId){
	$('#userDeleteId').val(userId);
	$('#userDeleteName').text($('#user-'+userId).text());
	$('#userDeleteConfirm').modal();
}

function userDelete(){
	var userDeleteId = $('#userDeleteId').val();
	
	$.ajax({
		url:'./userDelete',
		type:'POST',
		dataType: 'json',
		data: $("#deleteUser").serialize(),
		success: function(res){
			$('#tr-'+userDeleteId).remove();
			$('#userDeleteConfirm').modal('hide');
		},
		error: function(er){
			console.log(er);
		}
	});
}