<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class pageController extends Controller
{
    public function logInForm(){
		if(session()->has('user')){
			return redirect('main');
		}else{
			return view('logIn');
		}
	}
	
	public function logIn(Request $request){
		$user = User::where('email', $request->email)->get()->first();
		
		if($user){
			if(Hash::check($request->password, $user->password)){
				$role = Role::find($user->role_id);
				
				session()->put('user', $user->user);
				session()->put('email', $user->email);
				session()->put('role', $role->name);
				
				return redirect('main');
			}else{
				session()->flash('error', 'La contraseña ingresada es incorrecta');
			}
		}else{
			session()->flash('error', 'El mail ingresado es incorrecto');
		}
		
		return redirect('/');
	}
	
	public function logOut(){
		session()->flush();
		session()->flash('message', 'Hasta la próxima');
		return redirect('/');
	}
	
	public function mainMenu(){
		return view('main');
	}
	
	public function users(){
		return view('users');
	}
	
	public function reports(){
		
		return view('reports');
	}
}
