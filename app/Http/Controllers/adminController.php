<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class adminController extends Controller
{
    public function getUsers(){
		$users = User::all();
		
		foreach($users as $user){
			$roleName = Role::where('id', $user->role_id)->get()->first()->name;
			$user->roleName = $roleName;
		}
		
		return response()->json(['users' => $users]);
	}
	
	public function getRoles(){
		$roles = Role::all();
		
		return response()->json(['roles' => $roles]);
	}
	
	public function getUserById(){
		$user = User::find(Input::get('userId'));
		
		return response()->json(['user' => $user]);
	}
	
	public function userUpdate(Request $request){
		try{
			if($request->userEditPassword != $request->userEditRePassword)
				return response()->json(['error' => true, 'message' => 'Las contraseñas no coinciden']);
			
			$user = User::find($request->userEditId);
			
			$user->user = $request->userEditName;
			$user->email = $request->userEditEmail;
			$user->role_id = $request->userEditRole;
			
			if($user->password != $request->userEditPassword)
				$user->password = Hash::make($request->userEditPassword);
			
			$user->save();
			
			$user->roleName = Role::where('id', $user->role_id)->get()->first()->name;
			
			return response()->json(['error' => false, 'user' => $user]);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
	
	public function userDelete(Request $request){
		try{
			User::where('id', $request->userDeleteId)->delete();
			
			return response()->json(['state' => 'success']);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
	
	public function userSave(Request $request){
		try{
			$user = User::where('email', $request->userEmail)->get();
			if(count($user) > 0)
				return response()->json(['error' => true, 'message' => 'El usuario ya está registrado']);
			
			if($request->userPassword != $request->userRePassword)
				return response()->json(['error' => true, 'message' => 'Las contraseñas no coinciden']);
			
			$user = new User();
			
			$user->user = $request->userName;
			$user->email = $request->userEmail;
			$user->password = Hash::make($request->userPassword);
			$user->role_id = $request->userRole;
			
			$user->save();
			
			$user = User::orderBy('id', 'desc')->get()->first();
			$user->roleName = Role::where('id', $user->role_id)->get()->first()->name;
			
			return response()->json(['error' => false, 'user' => $user]);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
}
