<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Report;
use App\User;

class employeeController extends Controller
{
    public function getReportsByUserId(){
		$userId = User::where('email', session()->get('email'))->get()->first()->id;
		$reports = Report::where('user_id', $userId)->get();
		
		return response()->json(['reports' => $reports]);
	}
	
	public function getReportById(){
		$report = Report::find(Input::get('reportId'));
		
		return response()->json(['report' => $report]);
	}
	
	public function reportUpdate(Request $request){
		try{
			$report = Report::find($request->reportId);
			$report->description = $request->reportEditDescription;
			$report->save();
			
			return response()->json(['state' => 'success']);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
	
	public function reportDelete(Request $request){
		try{
			Report::where('id', $request->reportId)->delete();
			
			return response()->json(['state' => 'success']);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
	
	public function saveNewReport(Request $request){
		try{
			$date = date('Y-m-d_H:i:s');
			
			$report = new Report();
			
			$report->description = $request->reportDescription;
			$report->date = $date;
			$report->user_id = User::where('email', session()->get('email'))->get()->first()->id;
			
			$report->save();
			
			$reportId = Report::orderBy('id', 'desc')->get()->first()->id;
			
			return response()->json(['reportId' => $reportId, 'date' => $date]);
		}catch(Exception $ex){
			return response()->json(['state' => 'error']);
		}
	}
}
